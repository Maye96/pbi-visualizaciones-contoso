# PBI Visualizaciones Contoso


## Contoso DW - Visualizacion de datos
Empresa de ventas ficticias

## Description
Contoso es una empresa ficticia de venta en línea que está trabajando para intentar alcanzar sus objetivos de ventas.

El conjunto de datos, se compone de varios archivos:
* FactStrategyPlan: Contiene datos sobre las transacciones de venta de la empresa
* DimAccount
    Cuenta donde se indica el tipo de ingresos y egresos
* DimDate
    Detalles de fechas relacionadas a FactStrategyPlan
* DimProductCategory:
    Los tipos de productos existentes son:
    - Audio
    - TV and Video
    - Computers
    - Cameras and camcorders
    - Cell phones
    - Music, Movies and Audio Books
    - Games and Toys
    - Home Appliances
* DimScenario:
    Posee los escenarios relacionados a FactStrategyPlan: Actual, Budget y Forecast

El objetivo es realizar diferentes visualizacion

![](img/modelo.png)

## Graficas

- Donut Chart 
![](img/DonutChart.png)
El monto total de ventas durantes los años 2017-2018-2019 es de 53.64 donde el mayor porcentajes de productos vendidos es la categoria Home Appliances con el 31% seguida de computadores con el 25.14%. Los productos que menos se venden son Audio; Music, Movies and Audio Books y Games and Toys

- Tree Maps
![](img/TreeMaps.png)
El  gráfico, permite visualizar de forma rapida, sencilla y jerarquica a traves del tamaño de las celdas que las categorias mas demandas son Home Appliances, Computers, Cameras and camcorders

- Bar Chart
![](img/BarChart.png)
Este gráfico, posee jerarquización 
En la primera se visualiza un descenso del monto total de las ventas obtenidas, sin embargo al filtrar por la categorias Audio se ve un incremento
![](img/BarChart-Audio.png)

- Sales chart
![](img/VentasCharts.png)
A partir de la gráfica de columnas agrupadas se puede ver las diferentes categorias en el transcurso de los años y al seleccionar la barra deseada se ven en las tarjetas el total de monto: Actual, Presupuesto(Budget) y Pronóstico(Forecast), lo cual responde a si se cumplio el monto pronósticado

## Importante
Fue necesario ajustar el formato de datekey para que DimDate se pudiera relacionar de forma correcta con FactStrategyPlan ya que una poseia el formato mm/dd/aaaa y el otro dd/mm/aaaa

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
